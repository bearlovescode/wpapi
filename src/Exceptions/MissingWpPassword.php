<?php
    namespace Bearlovescode\WpApi\Exceptions;

    class MissingWpPassword extends \Exception
    {}