<?php
    namespace Bearlovescode\WpApi;

    class Configuration
    {
        private $_vars = [];

        public function __get($name)
        {
            if (isset($this->_vars[$name]) && !empty($this->_vars[$name]))
            {
                return $this->_vars[$name];
            }

            return null;
        }

        public function __set($name, $value)
        {
            $this->_vars[$name] = $value;
        }

        public function __isset($name)
        {
            return isset($this->_vars[$name]);
        }
    }