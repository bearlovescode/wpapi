<?php
    namespace Bearlovescode\WpApi;

    use Bearlovescode\WpApi\Exceptions\BadWpLogin;
    use Bearlovescode\WpApi\Exceptions\MissingWpPassword;
    use Bearlovescode\WpApi\Exceptions\MissingWpUsername;
    use Bearlovescode\WpApi\Exceptions\NotImplemented;
    use Bearlovescode\WpApi\Exceptions\NoWpCredentials;
    use Bearlovescode\WpApi\Models\WpPost;
    use GuzzleHttp\TransferStats;

    class WpApiClient extends ApiClient
    {
        private $_vars = [
            'username' => null,
            'password' => null,
            'uri' => null,
            'verb' => null
        ];

        private $_credentials;

        public function __get($name)
        {
            return $this->_vars[$name];
        }

        public function __set($name, $value)
        {
            $this->_vars[$name] = $value;
        }

        /**
         * WpApiClient constructor.
         * @param $url
         * @param $username
         * @param $password
         * @throws MissingWpPassword
         * @throws MissingWpUsername
         * @throws BadWpLogin
         * @throws NoWpCredentials
         * @throws \GuzzleHttp\Exception\GuzzleException
         */
        public function __construct(Configuration $config)
        {
            parent::__construct($config);

            if (!isset($config->apiUsername) || empty($config->apiUsername))
            {
                throw new MissingWpUsername();
            }

            if (!isset($config->apiPassword) || empty($config->apiPassword))
            {
                throw new MissingWpPassword();
            }

            $this->_vars['username'] = $config->apiUsername;
            $this->_vars['password'] = $config->apiPassword;

            if (isset($config->apiLoginMethod) && !empty($config->apiLoginMethod))
            {
                $this->login($config->apiLoginMethod);
            }

            else
            {
                $this->login();
            }


        }

        /**
         * @param string $method
         * @return void
         * @throws BadWpLogin
         */
        public function login($method = 'jwt') {
            $loginMethod = '_login_' . $method;
            if (!method_exists($this, $loginMethod))
            {
                throw new BadWpLogin('Method ' . $method . ' for login is not implemented.');
            }

            $this->$loginMethod();
        }

        /**
         * @throws BadWpLogin
         * @throws NoWpCredentials
         * @throws \GuzzleHttp\Exception\GuzzleException
         */
        protected function _login_jwt()
        {
            $this->addFormData([
                'username' => $this->username,
                'password' => $this->password
            ]);

            $response = $this->query('/jwt-auth/v1/token', 'post');

            if (!$response->getBody()) {
                throw new BadWpLogin();
            }

            $this->_credentials = json_decode($response->getBody());
        }

        /**
         * @throws NotImplemented
         */
        protected function _login_cookies()
        {
            throw new NotImplemented();
        }

        protected function _login_basic() {
            //$this->setAuthentication();
            $this->setAuthorization();

            //$response = $this->query('/');
        }

        public function getCredentials()
        {
            return $this->_credentials;
        }

        /**
         * @param null $uri
         * @param string $verb
         * @return mixed
         * @throws NoWpCredentials
         * @throws \GuzzleHttp\Exception\GuzzleException
         */
        public function query($uri = null, $verb = 'get')
        {
            if (isset($this->_credentials) && !empty($this->_credentials->token))
            {
                $this->addHeader('Authorization', 'Bearer ' . $this->_credentials->token);
            }

            return parent::query($uri, $verb);
        }

//        /**
//         * @throws NoWpCredentials
//         */
//        public function listUsers() {
//            if (!isset($this->_credentials) || empty($this->_credentials))
//            {
//                throw new NoWpCredentials();
//            }
//
//            $headers = array_merge($this->baseHeaders, [
//               'Authentication', 'Bearer ' . $this->_credentials->token
//            ]);
//            $data = null;
//
//            $response = $this->http->get((string) $this->getApiUrl('/wp/v2/users'), $headers);
//
//            if ($response->getBody())
//            {
//                $data = json_decode($response->getBody());
//            }
//
//            return $data;
//        }
//
//
//        public function getPosts() {
//            if (!isset($this->_credentials) || empty($this->_credentials))
//            {
//                throw new NoWpCredentials();
//            }
//
//            $headers = array_merge($this->baseHeaders, [
//                'Authentication', 'Bearer ' . $this->_credentials->token
//            ]);
//            $data = null;
//            $result = null;
//
//            $response = $this->http->get((string) $this->getApiUrl('/wp/v2/posts'), $headers);
//
//            if ($response->getBody())
//            {
//                $data = json_decode($response->getBody());
//            }
//
//            if (is_object($data))
//            {
//                $result = new WpPost($data);
//            }
//
//            if (is_array($data))
//            {
//                $result = [];
//                foreach($data as $postData)
//                {
//                    $result[] = new WpPost($postData);
//                }
//            }
//
//
//            return $result;
//        }
//
//        public function createPost(WpPost $thePost) {
//            if (!isset($this->_credentials) || empty($this->_credentials))
//            {
//                throw new NoWpCredentials();
//            }
//
//            $headers = array_merge($this->baseHeaders, [
//                'Authentication', 'Bearer ' . $this->_credentials->token,
//                'field_params' => (array) $thePost
//            ]);
//            $data = null;
//            $result = null;
//
//            $response = $this->http->post((string) $this->getApiUrl('/wp/v2/posts'), $headers);
//
//            if ($response->getBody())
//            {
//                $data = json_decode($response->getBody());
//            }
//
//            var_dump($result);
//
//
//            return $result;
//        }


    }
