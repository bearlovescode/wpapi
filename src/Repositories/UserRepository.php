<?php
    namespace Bearlovescode\WpApi\Repositories;

    use Bearlovescode\WpApi\Models\WpUser;

    class UserRepository extends _ApiRepository
    {
        /**
         * return the currently auth user record.
         */
        public function me() {
            $result = null;

            $response = $this->client->query('/wp/v2/users/me');

            if ($response->getBody())
            {
                $data = json_decode($response->getBody());
                $result = new WpUser($data);
            }

            return $result;
        }
    }