<?php
    namespace Bearlovescode\WpApi\Repositories;

    use Bearlovescode\WpApi\Configuration;
    use Bearlovescode\WpApi\WpApiClient;

    class _ApiRepository
    {
        protected $client;

        /**
         * _ApiRepository constructor.
         * @throws \Bearlovescode\WpApi\Exceptions\MissingWpPassword
         * @throws \Bearlovescode\WpApi\Exceptions\MissingWpUsername
         * @throws \Bearlovescode\WpApi\Exceptions\BadWpLogin
         */
        public function __construct(Configuration $config)
        {
            $this->client = new WpApiClient($config);
        }
    }