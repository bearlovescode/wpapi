<?php
    namespace Bearlovescode\WpApi\Repositories;

    use Bearlovescode\WpApi\Models\WpPost;
    use Bearlovescode\WpApi\Models\WpUser;
    use GuzzleHttp\Exception\ClientException;
    use GuzzleHttp\Exception\RequestException;

    class PostRepository extends _ApiRepository
    {
        public function all() {
            $result = null;

            $response = $this->client->query('/wp/v2/posts');

            if ($response->getBody())
            {
                $data = json_decode($response->getBody());

                if (is_array($data))
                {
                    $result = [];
                    foreach ($data as $record)
                    {
                        $result[] = new WpPost($record);
                    }
                }

                else
                {
                    $result = new WpPost($data);
                }
            }

            return $result;
        }

        /**
         * @param $id
         * @return WpPost|null
         * @throws \Bearlovescode\WpApi\Exceptions\NoWpCredentials
         * @throws \GuzzleHttp\Exception\GuzzleException
         */
        public function find($id)
        {
            $result = null;
            try
            {
                $response = $this->client->query('/wp/v2/posts/' . $id);

                if ($response->getBody())
                {
                    $data = json_decode($response->getBody());
                    $result = new WpPost($data);
                }
            }

            catch(ClientException $e)
            {

            }

            return $result;
        }

        public function save(WpPost $post)
        {
            $this->client->addFormData($post->toArray());

            try
            {
                $response = $this->client->query('/wp/v2/posts', 'post');

                if ($response->getBody())
                {
                    $result = json_decode($response->getBody());
                }
            }

            catch(ClientException $e)
            {
                var_dump($e->getMessage());
            }

            return $result;
        }
    }