<?php
    namespace Bearlovescode\WpApi\Models;

    class WpCategory extends ApiModel
    {
        protected $_fields = [
            'id' => null,
            'count' => null,
            'description' => null,
            'link' => null,
            'name' => null,
            'slug' => null,
            'taxonomy' => null,
            'parent' => null,
            'meta' => null
        ];
    }