<?php
namespace Bearlovescode\WpApi\Models;

class WpUser extends ApiModel
{
    protected $_fields = [
        'id' => null,
        'username' => null,
        'name' => null,
        'first_name' => null,
        'last_name' => null,
        'email' => null,
        'url' => null,
        'description' => null,
        'link' => null,
        'locale' => null,
        'nickname' => null,
        'slug' => null,
        'registered_date' => null,
        'roles' => [],
        'password' => null,
        'capabilities' => null,
        'extra_capabilities' => null,
        'avatar_urls' => null,
        'meta' => null
    ];
}