<?php
    namespace Bearlovescode\WpApi\Models;

    class ApiModel
    {
        protected $_fields = [];

        /**
         * @param $name
         * @return mixed|null
         */
        public function __get($name)
        {
            $result = null;
            if (isset($this->_fields[$name]) && !empty($this->_fields[$name]))
            {
                $result = $this->_fields[$name];
            }

            return $result;
        }

        public function __set($name, $value)
        {
            $this->_fields[$name] = $value;
        }

        public function __construct($data = null)
        {
            if (isset($data) && is_object($data))
            {
                $this->hydrate($data);
            }
        }

        private function hydrate($data)
        {
            foreach(array_keys($this->_fields) as $fld)
            {
                if (property_exists($data, $fld))
                {
                    $this->_fields[$fld] = $data->$fld;
                }
            }
        }

        public function toArray()
        {
            return $this->_fields;
        }
    }