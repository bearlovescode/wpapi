<?php
    namespace Bearlovescode\WpApi\Models;

    class WpPost extends ApiModel
    {
        protected $_fields = [
            'password' => '',
            'date' => null,
            'slug' => null,
            'status' => null,
            'title' => null,
            'content' => null,
            'author' => null,
            'excerpt' => null,
            'featured_media' => null,
            'comment_status' => 'closed',
            'ping_status' => 'closed',
            'format' => 'standard',
            'meta' => null,
            'sticky' => false,
            'categories' => null,
            'tags' => null
        ];
    }