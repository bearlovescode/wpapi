<?php
    namespace Bearlovescode\WpApi;

    use Bearlovescode\WpApi\Helpers\Url;
    use GuzzleHttp\Client;
    use GuzzleHttp\Exception\ClientException;
    use GuzzleHttp\Exception\RequestException;

    class ApiClient
    {
        protected $http;
        protected $cookies;
        protected $baseHeaders = [];
        protected $configuration;

        private $_clientVars = [
            'baseUrl' => null,
            'verb' => 'get',
            'form_params' => [],
            'headers' => []
        ];

        public function __get($name)
        {
            if (isset($this->_clientVars[$name]) && !empty($this->_clientVars[$name]))
            {
                return $this->_clientVars[$name];
            }

            return null;
        }

        public function __set($name, $value)
        {
            $this->_clientVars[$name] = $value;
        }



        public function __construct(Configuration $config)
        {
            $this->configuration = $config;
            $this->_clientVars['baseUrl'] = $config->apiUrl;

            $this->http = new Client();
        }


        /**
         * @param null $uri
         * @param string $verb
         * @return mixed|\Psr\Http\Message\ResponseInterface
         * @throws \GuzzleHttp\Exception\GuzzleException
         */
        public function query($uri = null, $verb = 'get')
        {
            $options = [
               'debug' => true
            ];
            if (isset($this->_clientVars['headers']) && !empty($this->_clientVars['headers']))
            {
                $options['headers'] = $this->_clientVars['headers'];
            }

            if (isset($this->_clientVars['form_params']) && !empty($this->_clientVars['form_params']))
            {
                $options['form_params'] = $this->_clientVars['form_params'];
            }
            $url = (string) $this->getApiUrl($uri);
            $response = $this->http->request($verb, $url, $options);
            $this->clearFormData();
            return $response;

        }

        public function getVerb()
        {
            return isset($this->_clientVars['verb']) ? strtolower($this->_clientVars['verb']) : 'get';
        }

        /**
         * @param $uri
         * @return Url|null
         */
        public function getApiUrl($uri)
        {
            if (!is_null($uri))
            {
                if (false === strpos($uri, '/', 0))
                {
                    $uri = '/' . $uri;
                }
            }

            $path = $this->_clientVars['baseUrl'] . $uri;
            return new Url($path);
        }

        public function setBaseUrl($value)
        {
            $this->_clientVars['baseUrl'] = $value;
        }

        public function addFormData($value)
        {
            if (is_array($value)) {
                $this->_clientVars['form_params'] = array_merge($value, $this->_clientVars['form_params']);
            }
        }

        public function getFormData()
        {
            return $this->_clientVars['form_params'];
        }

        public function clearFormData()
        {
            $this->_clientVars['form_params'] = [];
        }


        public function addHeader($k, $v)
        {
            $this->_clientVars['headers'][$k] = $v;
        }

        public function getHeaders()
        {
            return $this->_clientVars['headers'];
        }

        public function setHeaders($headers = [])
        {
            $this->_clientVars['headers'] = $headers;
        }

        public function setAuthentication()
        {
            $basicToken = base64_encode(trim($this->configuration->apiUsername) . ':' . trim($this->configuration->apiPassword));
            $this->addHeader('Authentication', 'Basic ' . $basicToken);
        }

        public function setAuthorization()
        {
            $basicToken = base64_encode(trim($this->configuration->apiUsername) . ':' . trim($this->configuration->apiPassword));
            $this->addHeader('Authorization', 'Basic ' . $basicToken);
        }


    }